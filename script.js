/* 
Теоретичні питання
1. Як можна визначити, яку саме клавішу клавіатури натиснув користувач?

визначити за допомогою властивості event.code чи event.key.

2. Яка різниця між event.code() та event.key()?

Властивість key об'єкта події дозволяє отримати символ, тоді як властивість
code об'єкта події дозволяє отримати "фізичний код клавіші"

3. Які три події клавіатури існує та яка між ними відмінність?

keydown виникає, коли клавішу натиснуто 
keyup - коли клавішу відпускають.
keypress - коли натискається клавіша яка створює символ

*/

// Практичне завдання.
// Реалізувати функцію підсвічування клавіш.

// Технічні вимоги:

// - У файлі index.html лежить розмітка для кнопок.
// - Кожна кнопка містить назву клавіші на клавіатурі
// - Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.
// Але якщо при натискані на кнопку її  не існує в розмітці, то попередня активна кнопка повина стати неактивною.


// function buttonPressActiv (event) {

//     for (let i = 0; i <= 7; i++){

//         let arrBtn = document.querySelectorAll('button.btn')[i].textContent;
//         let arrBtnColor = document.querySelectorAll('.btn')[i];
//         // console.log(arrBtn)
//         // console.log(arrBtnColor)
       
//         if (event.key === arrBtn){
//             // console.log(event)
//             arrBtnColor.classList.add('active')
//         }else{
//             arrBtnColor.classList.remove('active')
//         }
        
//     }

// }

// document.addEventListener('keydown', buttonPressActiv)

const btnKey = document.getElementsByClassName('btn');

function activeBtn (btn){
    document.addEventListener('keydown', (event) =>{
        for(let key of btn){
            if((event.code === key.textContent) 
            || (event.key === key.textContent.toLowerCase())){
        key.classList.add('btn__active')
            }else{
                key.classList.remove('btn__active')

        }
        }
    })
}

activeBtn(btnKey)
